Po spusteni aplikacie mame na vyber dve moznosti. Prva je nacitat bludisko zo suboru a druha je nacitat bludisko z klavesnice. 
Pri prvej moznosti netreba zadavat nic, pri druhej treba zadat velkost bludiska a potom vypisat jednotlive polia. 
Co sa tyka textoveho suboru tak treba ho mat vo formate: prve dve cisla su rozmer matice a potom nacitava znaky oddelene medzerou. 
Ospravedlnujem sa za nedokoncene komentare v kode ale nestihal som kvoli skole. 
Taktiez som dobre neosetril vstupy, snazil som sa len okrajovo aby program nepadol. 
Rad by som ulohu vypracoval lepsie ale bohuzial zajtra som v skole a pracu musim odovzdat skor.

Algoritmus funguje spravne.

input: 

# # # # # # 
. S # . # . 
. . # . . . 
. . . . . . 
. # # . # . 
. # # . # X 
output: 
S -> d,d,r,r,r,r,d,d, -> X