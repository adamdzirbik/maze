/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayDeque;
import java.util.Queue;

/**
 *
 * @author Adam
 */
public class Maze {
    private int M;
    private int N;
    
    public Maze(int M, int N) {
        this.M = M;
        this.N = N;
    }

    // Below arrays details all 4 possible movements from a cell
    private static final int row[] = { -1, 0, 0, 1 };
    private static final int col[] = { 0, -1, 1, 0 };
    
    private boolean isValid(int mat[][], boolean visited[][],
                                   int row, int col)
    {
        return (row >= 0) && (row < this.M) && (col >= 0) && (col < this.N)
                && mat[row][col] == 1 && !visited[row][col];
    }
    
    public void findBestPath(int mat[][], int i, int j, int x, int y)
    {
        // construct a matrix to keep track of visited cells
        boolean[][] visited = new boolean[M][N];

        // create an empty queue
        Queue<Element> q = new ArrayDeque<>();

        // mark source cell as visited and enqueue the source node
        visited[i][j] = true;
        q.add(new Element(i, j, 0,' ', null));

        // stores length of longest path from source to destination
        int min_dist = Integer.MAX_VALUE;
        Element element = null;
        // run till queue is not empty
        while (!q.isEmpty())
        {
            // pop front node from queue and process it
            element = q.poll();

            // (i, j) represents current cell and dist stores its
            // minimum distance from the source
            i = element.getX();
            j = element.getY();
            int dist = element.getDist();

            // if destination is found, update min_dist and stop
            if (i == x && j == y)
            {
                min_dist = dist;
                break;
            }

            // check for all 4 possible movements from current cell
            // and enqueue each valid movement
            for (int k = 0; k < 4; k++)
            {
                // check if it is possible to go to position
                // (i + row[k], j + col[k]) from current position
                if (isValid(mat, visited, i + row[k], j + col[k]))
                {
                    // mark next cell as visited and enqueue it
                    visited[i + row[k]][j + col[k]] = true;
                    if(col[k] == 1){
                       q.add(new Element(i + row[k], j + col[k], dist + 1,'r',element));    // right
                    } else if(col[k] == -1){
                       q.add(new Element(i + row[k], j + col[k], dist + 1,'l',element));    // left
                    } else if (row[k] == 1) {
                       q.add(new Element(i + row[k], j + col[k], dist + 1,'d',element));    // down
                    } else {
                       q.add(new Element(i + row[k], j + col[k], dist + 1,'u',element));    // up1
                    }
                        
                }
            }
        }
        
        if (min_dist != Integer.MAX_VALUE) {
            System.out.println("output: ");
            printPath(element);
            System.out.println(" -> X");
        }
        else {
            System.out.println("Destination can't be reached from source");
        }
    }

    private void printPath(Element element) {
        if (element == null) {
            System.out.print("S -> ");
            return;
        }
        printPath(element.getParent());
        if(element.getSmer()!= ' '){
            System.out.print(element +",");
        } 
    }
}
