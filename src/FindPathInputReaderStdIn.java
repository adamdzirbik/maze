
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class FindPathInputReaderStdIn extends AbstractFindPathInputReader {
        private int[][] array;
        private String[][] printArray;   
    @Override
    public void loadMaze() {
    try { 
        Scanner scanS = new Scanner(System.in);
        System.out.println("Enter MxN maze: ");    
        
        int a = scanS.nextInt();
        int b = scanS.nextInt();
        
        super.m = new Maze(a, b);
        this.array = new int[a][b];
        this.printArray = new String[a][b];
        
        System.out.println("Element options are '.' - free, '#' - blocked, 'S' - start position,'X' - end position."); 
        
        
        for(int i=0;i<printArray.length;i++){
            for(int j=0;j<printArray.length;j++){ 
                System.out.println("Enter element on position["+i+"]"+"["+j+"]:");
                this.printArray[i][j] = scanS.next();
            }
        }
    } catch(Exception e){
        System.out.println("Incorrect symbol used.");
    }
}
   
    
    @Override
    public void printMaze(){
    
        System.out.println("Input: ");
        for(int i=0;i<printArray.length;i++){
            for(int j=0;j<printArray.length;j++){
                if(j%(this.printArray.length) == 0){
                    System.out.println();
                }
                System.out.print(this.printArray[i][j] + " ");
            }
        }
        convertMaze();
    
    }
    
    
    public void convertMaze(){
        this.startP = new int[2];
        this.endP = new int[2];
        for(int i=0;i < printArray.length; i++) {
            for(int j=0;j < printArray.length;j++) {
                switch (this.printArray[i][j]) {
                    case "S":
                        this.array[i][j] = 1;
                        this.startP[0] = i;
                        this.startP[1] = j;
                        break;
                    case "X":
                        this.array[i][j] = 1;
                        this.endP[0] = i;
                        this.endP[1] = j;
                        break;
                    case "#":
                        this.array[i][j] = 0;
                        break;
                    default:
                        this.array[i][j] = 1;
                        break;
                }
            }
        }
    }
    
    @Override
    public int getStartPosition(int i) {
        return this.startP[i];
    }
    
    @Override
    public int getEndPosition(int i) {
        return this.endP[i];
    }
    
    @Override
    public int[][] getMaze() {
        return this.array;
    }
    
    @Override
    public Maze getM(){
        return this.m;
    }

    
}