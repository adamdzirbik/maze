
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class FindPathInputReader extends AbstractFindPathInputReader {
    private int[][] array;   
    private char[][] printArray;
    
    
    @Override
    public void loadMaze() {
        try {
            File maze = new File("maze.txt"); 
            Scanner scanMaze = new Scanner(maze);  
            
            int a = scanMaze.nextInt();
            int b = scanMaze.nextInt();
            m = new Maze(a,b);
            
            this.array= new int[a][b];
            this.printArray= new char[a][b];
            this.startP = new int[2];
            this.endP = new int[2];
            
            for(int i=0;i < array.length; i++) {
                for(int j=0;j < array.length;j++) {
                    switch (scanMaze.next().charAt(0)) {
                        case 'S':
                            this.array[i][j] = 1;
                            this.printArray[i][j] = 'S';
                            this.startP[0] = i;
                            this.startP[1] = j;
                            break;
                        case 'X':
                            this.array[i][j] = 1;
                            this.printArray[i][j] = 'X';
                            this.endP[0] = i;
                            this.endP[1] = j;
                            break;
                        case '#':
                            this.array[i][j] = 0;
                            this.printArray[i][j] = '#';
                            break;
                        default:
                            this.array[i][j] = 1;
                            this.printArray[i][j] = '.';
                            break;
                    }
                }
            }
            scanMaze.close();
        } catch (IOException e){
                System.out.println("Incorrect symbol used.");
       }
            
    }
    
    @Override
    public void printMaze() {
        System.out.println("input: ");
        for(int i=0;i<array.length;i++){
            for(int j=0;j<array.length;j++){
                if(j%(this.array.length) == 0){
                    System.out.println();
                }
                System.out.print(this.printArray[i][j]+ " ");
            }
        }
    }
    @Override
    public int getStartPosition(int i) {
        return this.startP[i];
    }
    
    @Override
    public int getEndPosition(int i) {
        return this.endP[i];
    }
    
    @Override
    public int[][] getMaze() {
        return this.array;
    }
    
    @Override
    public Maze getM(){
        return this.m;
    }
}
