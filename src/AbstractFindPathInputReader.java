/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
abstract class AbstractFindPathInputReader {
         
    
    protected int[] startP, endP;
    protected Maze m;
    
    public abstract void loadMaze();
    public abstract void printMaze();
    public abstract int getStartPosition(int i);
    public abstract int getEndPosition(int i);
    public abstract Maze getM();
    public abstract int[][] getMaze();
}
