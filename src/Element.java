/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class Element {
    
    private int x, y, dist;
    private char smer;
    // maintain a parent node for printing path
    private Element parent;

    Element(int x, int y, int dist,char smer, Element parent) {
        this.x = x;
        this.y = y;
        this.dist = dist;
        this.smer = smer;
        this.parent = parent;
        
    }

    public char getSmer() {
        return smer;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getDist() {
        return dist;
    }

    public Element getParent() {
        return parent;
    }

    @Override
    public String toString() {
        
        return smer+"";
    }
}