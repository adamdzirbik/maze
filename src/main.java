
import java.util.NoSuchElementException;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Adam
 */
public class main {
    private static AbstractFindPathInputReader fp;
    public static void main(String [] args)  {
        
        
        
        System.out.println("Press 1 if you want to use input from file or 2 for standard input.");
        System.out.println("Press any other number to close the program.");
        String input;
        Scanner scan = new Scanner(System.in);
        try {
            boolean loop = true;
            
            while(loop) {
                if(scan.hasNext()){
                    input =scan.next();
                } else {
                    input = "0";
                }
                
                switch(input) {
                    case "1": 
                        fp = new FindPathInputReader();
                        fp.loadMaze();
                        fp.printMaze();
                        System.out.println();
                        fp.getM().findBestPath(fp.getMaze(), fp.getStartPosition(0), fp.getStartPosition(1), fp.getEndPosition(0), fp.getEndPosition(1));
                        break;
                    case "2":
                        fp = new FindPathInputReaderStdIn();
                        try {
                            fp.loadMaze();
                        }catch (Exception e){}
                        fp.printMaze();
                        System.out.println();
                        fp.getM().findBestPath(fp.getMaze(), fp.getStartPosition(0), fp.getStartPosition(1), fp.getEndPosition(0), fp.getEndPosition(1));
                        break;
                    default:
                        loop = false;
                        break;
                }
                
            }
            System.out.println("Program has been closed!");
        }catch(Exception e) {
           System.out.println("Something is wrong.");
        } finally{
            scan.close();
        }
    }
}

